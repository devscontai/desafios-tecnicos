

# Desafio Técnico Pessoa Desenvolvedora Backend

Olá! 
Nós da ContAí agradecemos imensamente pelo seu interesse! Estou confiante que juntos vamos construir algo impactante para a sociedade brasileira.

## Descrição do Desafio

Para este desafio técnico, você vai criar uma API hipotética de calculadora para empresas. Esta API terá endpoints para executar as 4 operações matemáticas basicas: soma, subtração, multiplicação, divisão inteira. Cada operação executada pelo usuário deve ser registrada no banco de dados, e um endpoint deve existir para exibir todas as solicitações feitas anteriormente.

## Performance 
Esta calculadora será executada simultaneamente por milhões de pessoas, então performance é muito importante. Pensar em estratégias de cache e na arquitetura da informação de forma que a aplicação seja escalável - isso pode ser entregue em um documento a parte, não necessariamente precisa ser implementado - porém se você utilizar docker-compose para estruturar a aplicação não seria tão difícil assim fazer.

## Modelagem
Do ponto de vista DDD, você é totalmente livre para criar as abstrações, substantivos e ações que as entidades irão desempenhar na execução do sistema. 
Para nós, mais importante do que o código em si é a elegância do design da arquitetura - não sua complexidade.

## O que seria interessante entregar

* Codigo com uma boa qualidade, em um repositório no github
* Documentação swagger de como utilizar a API
* Dockerfile/docker-compose para executar a aplicação
* Qualquer desenho ou diagrama que ilustre as decisões de design


Por favor retorne esse email confirmando leitura e o prazo estimado para entrega do desafio. Eu não gostaria que voce gastasse mais do que uma tarde na execução do mesmo, minha intenção não é que voce entregue algo exageradamente complexo.

Depois da sua entrega vamos marcar mais uma conversa, super leve, para discutirmos as suas decisões e propor uma simples modificação da API para desenvolvermos juntos.

Boa sorte!
