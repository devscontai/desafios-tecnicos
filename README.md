

* Utilizar cada pasta para separar os tipos de desafios tecnicos necessários
* Criar um arquivo markdown com as especificações necessárias e gerar um PDF através da ferramenta [Dillinger](https://dillinger.io/)
* Enviar para o candidato o PDF por email e uma mensagem amigável. Sugestão:

```

Título da Mensagem
----------
Fulaninho de Tal - Pessoa Desenvolvedora Elixir - Desafio Técnico ContAí


Conteúdo da Mensagem
----------

Olá! Seja bem vindx ao time! 

Este desafio técnico tem o objetivo de nos fazer entender um pouquinho mais o seu processo de pensamento durante o trabalho. Entendo que a gente só conhece um ao outro mesmo ali no calor do dia a dia, mas esse processo intelectual é importante nesse primeiro momento. 

Sendo assim não se preocupe muito com preciosismo excessivo, separe uma janela de tempo e implemente o que for possível. Ademais, você pode criar um documento markdown com os próximos passos, e como uma arquitetura mais avançada poderia ser.

Muito obrigado pela atenção, a descrição do exercício está em anexo, qualquer dúvida pode responder esse e-mail.

Um fraternal abraço,



```

