
# Desafio Técnico Pessoa Desenvolvedora React Native

Olá! 
Nós da ContAí agradecemos imensamente pelo seu interesse! Estou confiante que juntos vamos construir algo impactante para a sociedade brasileira.


## Descrição do Desafio

Para este desafio técnico, você vai criar um módulo de reuniões que faz parte de um CRM hipotético para que os gestores possam ter uma visão das atividades dos funcionários da empresa. Este módulo é batizado como **Meetings**.


## Requisitos do Projeto

* O usuário poderá adicionar uma reunião, com os seguintes dados:
    * Objetivo da Reunião
    * Horário
    * Foto para referencia (opcional)
    * Colaboradores envolvidos
 
* O usuário poderá deletar uma reunião
* O usuário poderá alterar os dados de uma reunião
* Se o usuário fechar o aplicativo e abrir novamente, os dados precisam ser salvos para serem acessados na próxima vez que abrir o aplicativo.
* Desenhos de interfaces, interface responsiva são totalmente opcionais. Não dedique muito tempo com isso, mas é interessante entregar uma interface minimamente apresentável.


## O que seria interessante entregar

* Codigo com uma boa qualidade, em um repositório no github
* Qualquer desenho ou diagrama que ilustre as decisões de modelagem da aplicação
* Utilizar Context API
* Ter uma estrutura para no futuro o aplicativo se comunicar com uma APIs.


Por favor retorne esse email confirmando leitura e o prazo estimado para entrega do desafio. Eu não gostaria que voce gastasse mais do que uma tarde na execução do mesmo, minha intenção não é que voce entregue algo exageradamente complexo.

Depois da sua entrega vamos marcar mais uma conversa, super leve, para discutirmos as suas decisões e propor uma simples modificação no aplicativo para desenvolvermos juntos.

Boa sorte!
